*** Settings ***
Library          SeleniumLibrary
Suite Setup      Set Selenium Speed    0.2s
Test Setup       Open Googleweb
Test Teardown    Close Browser
Suite Teardown   Close All Browsers

*** Variables ***
${url}               https://www.google.com/
${browser}           gc
${locator_search}    name=q
${locator_number2}   xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[4]/td[2]/div/div
${locator_number4}   xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[3]/td[1]/div/div
${locator_plus}      xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[5]/td[4]/div/div
${locator_minus}     xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[4]/td[4]/div/div
${locator_multiply}  xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[3]/td[4]/div/div      
${locator_divide}    xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[2]/td[4]/div/div 
${locator_ans}       xpath=//*[@id="rso"]/div[1]/div/div/div/div[1]/div/div/div[3]/div/table[2]/tbody/tr[5]/td[3]/div/div   
${locator_result}    id=cwos



*** Keywords ***
Open Googleweb
    [Documentation]    ทำการเปิด Webbrowser
    [Tags]             Openbrowser
    #เรียกใช้คำสั่ง Open Browser เพื่อทำการเปิด Google Chrome 
    # โดยการส่ง        url และ browser ที่จะใช้
    Open Browser    ${url}    ${browser}
    #ใช้คำสั่งเพื่อทำการขยายหน้าจอ
    Maximize Browser Window

Input Keyword for search
    [Documentation]    ใช้สำหรับกรอก keyword ที่ต้องการค้นหา
    [Tags]             Search
    #รอจนกว่า Field input Search ปรากฏขึ้นบนหน้าเว็บ
    Wait Until Element Is Visible    ${locator_search}
    #ทำการกรอก keyword             ใน Field input Search    ด้วยคำว่า
    Input Text                       ${locator_search}    calculator
    #สั่งให้ตัวโปรแกรมกด Enter อัตโนมัติ
    Press Keys                       ${locator_search}    ENTER
    #รอจนกว่าหน้าจอจะแสดงคำว่า calculator
    Wait Until Page Contains         calculator

Verify Check result calculator - plus
    [Documentation]    ใช้สำหรับกรณีบวกตัวเลข
    [Tags]    +
    #ให้โปรแกรมทำการกดปุ่มเลข4ในหน้าเว็บ
    Click Element    ${locator_number4}
    #ให้โปรแกรมทำการกดปุ่ม+ในหน้าเว็บ
    Click Element    ${locator_plus}
    #ให้โปรแกรมทำการกดปุ่มเลข2ในหน้าเว็บ
    Click Element    ${locator_number2}
    #ให้โปรแกรมทำการกดปุ่ม=ในหน้าเว็บ
    Click Element    ${locator_ans}
    #ทำการดึงข้อมูลผลลัพท์ที่ได้มาเก็บไว้ในตัวแปรที่ชื่อว่า ans
    ${ans}=    Get Text    ${locator_result} 
    #ทำการตรวจสอบค่าผลลัพท์ที่ได้โดยการเทียบกับคำตอบที่ถูกต้อง
    Should Be Equal As Integers    6    ${ans}  

Verify Check result calculator - minus
    [Documentation]    ใช้สำหรับกรณี - ตัวเลข
    [Tags]    minus
    #ให้โปรแกรมทำการกดปุ่มเลข4ในหน้าเว็บ
    Click Element    ${locator_number4}
    #ให้โปรแกรมทำการกดปุ่ม-ในหน้าเว็บ
    Click Element    ${locator_minus}
    #ให้โปรแกรมทำการกดปุ่มเลข2ในหน้าเว็บ
    Click Element    ${locator_number2}
    #ให้โปรแกรมทำการกดปุ่ม=ในหน้าเว็บ
    Click Element    ${locator_ans}
    #ทำการดึงข้อมูลผลลัพท์ที่ได้มาเก็บไว้ในตัวแปรที่ชื่อว่า ans
    ${ans}=    Get Text    ${locator_result} 
    #ทำการตรวจสอบค่าผลลัพท์ที่ได้โดยการเทียบกับคำตอบที่ถูกต้อง
    Should Be Equal As Integers    2    ${ans}  

Verify Check result calculator - multiply
    [Documentation]    ใช้สำหรับกรณี * ตัวเลข
    [Tags]    *
    #ให้โปรแกรมทำการกดปุ่มเลข4ในหน้าเว็บ
    Click Element    ${locator_number4}
    #ให้โปรแกรมทำการกดปุ่ม* ในหน้าเว็บ
    Click Element    ${locator_multiply}
    #ให้โปรแกรมทำการกดปุ่มเลข2ในหน้าเว็บ
    Click Element    ${locator_number2}
    #ให้โปรแกรมทำการกดปุ่ม=ในหน้าเว็บ
    Click Element    ${locator_ans}
    #ทำการดึงข้อมูลผลลัพท์ที่ได้มาเก็บไว้ในตัวแปรที่ชื่อว่า ans
    ${ans}=    Get Text    ${locator_result} 
    #ทำการตรวจสอบค่าผลลัพท์ที่ได้โดยการเทียบกับคำตอบที่ถูกต้อง
    Should Be Equal As Integers    8    ${ans}  
    
Verify Check result calculator - divide
    [Documentation]    ใช้สำหรับกรณี / ตัวเลข
    [Tags]    /
    #ให้โปรแกรมทำการกดปุ่มเลข4ในหน้าเว็บ
    Click Element    ${locator_number4}
    #ให้โปรแกรมทำการกดปุ่ม / ในหน้าเว็บ
    Click Element    ${locator_divide}
    #ให้โปรแกรมทำการกดปุ่มเลข2ในหน้าเว็บ
    Click Element    ${locator_number2}
    #ให้โปรแกรมทำการกดปุ่ม=ในหน้าเว็บ
    Click Element    ${locator_ans}
    #ทำการดึงข้อมูลผลลัพท์ที่ได้มาเก็บไว้ในตัวแปรที่ชื่อว่า ans
    ${ans}=    Get Text    ${locator_result} 
    #ทำการตรวจสอบค่าผลลัพท์ที่ได้โดยการเทียบกับคำตอบที่ถูกต้อง
    Should Be Equal As Integers    2    ${ans}  


*** Test Cases ***
TC001-Verify Open Webbrowser
    [Documentation]    ทดสอบการเปิด webbrowser
    Input Keyword for search
    Close Browser

TC002-Verify Check result calculator - plus
    [Documentation]    ทดสอบการคำนวณตัวเลข กรณี +
    Input Keyword for search
    Verify Check result calculator - plus

TC003-Verify Check result calculator - minus
    [Documentation]    ทดสอบการคำนวณตัวเลข กรณี -
    Input Keyword for search
    Verify Check result calculator - minus

TC004-Verify Check result calculator - multiply
    [Documentation]    ทดสอบการคำนวณตัวเลข กรณี *
    Input Keyword for search
    Verify Check result calculator - multiply

TC005-Verify Check result calculator - divide
    [Documentation]    ทดสอบการคำนวณตัวเลข กรณี /
    Input Keyword for search
    Verify Check result calculator - divide
